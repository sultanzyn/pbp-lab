from django.shortcuts import render
from lab_2.models import Note
from django.http.response import HttpResponse
from django.core import serializers
# Create your views here.
def index(request):
    notes = Note.objects.all() 
    response = {'Notes': notes}
    return render(request, 'lab5_index.html', response)