# Generated by Django 3.2.7 on 2021-09-16 15:57

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('lab_1', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='friend',
            name='dateOfBirth',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
        migrations.AddField(
            model_name='friend',
            name='npm',
            field=models.CharField(default=0, max_length=10),
            preserve_default=False,
        ),
    ]
