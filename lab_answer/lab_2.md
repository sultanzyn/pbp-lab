1. Apakah perbedaan antara JSON dan XML?
JSON dan XML merupakan pilihan data format yang bisa digunakan ketika ingin menyimpan data dari web kita. Perbedaan dari keduanya adalah:
    -Strukturnya
    JSON memiliki struktur berupa dictionary yang memiliki "key"-"value"
    XML memilikir struktur mirip seperti HTML yang berupa kumpulan tags yang disusun seperti pohon (ada root-leaf) namun bedanya tags pada XML tidak pre-defined. Nama dari tag akan sama dengan "key" pada JSON dan isi dari tag akan sama dengan "value"
    -Bahasa dibuatnya
    JSON dibuat berdasarkan JavaScript dan memang strukturnya merupakan kumpulan objek JavaScript
    XML merupakan bahasa sendiri
    -Tipe data yang disimpan 
    JSON, karena JSON dibuat dari JavaScript yang merupakan sebuah bahasa yang bisa memiliki tipe data yang berbeda. Data yang disimpan pada "value" JSON dapat memiliki tipe data terdefinisi .
    XML, sama seperti HTML selain dari tags yang membungkusnya data yang disimpan pada isi tag pada XML tidak memiliki tipe apapun dalam artian merupakan teks murni.

2. Apakah perbedaan antara HTML dan XML?
Walaupun baik HTML dan XML memiliki struktur yang serupa yakni kumpulan tags dan isi dari tags. Tujuan dari HTML dan XML sangat berbeda dimana HTML ditujukan sebagai kerangka dasar sebuah halaman yang akan ditampilkan di web sementara XML ditujukan untuk menyimpan data yang diterima dari web. HTML dapat ditranlasi oleh browser menjadi sebuah tampilan yang jauh berbeda dari script murni ketika HTML dibuat sementara XML tidak, ketika kita mencoba menaplikannya pada suatu halaman web, browser akan menampilkan hal yang sama seperti apa yang tertulis pada script XML tersebut.
