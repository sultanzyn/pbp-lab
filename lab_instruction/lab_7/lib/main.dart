import 'package:flutter/material.dart';

import 'home.dart';

void main() {
  runApp(MaterialApp(
    title: "SIAK IMIGRASI",
    home: SIAKImigrasi(),
  ));
}

class SIAKImigrasi extends StatelessWidget {
  const SIAKImigrasi({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData.dark(),
      home: HomeScreen(),
    );
  }
}
