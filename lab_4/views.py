from django.shortcuts import render
from lab_2.models import Note
from .forms import NoteForm
from django.http import HttpResponseRedirect

# Create your views here.


def index(request):
    notes = Note.objects.all() 
    response = {'Notes': notes}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    form  = NoteForm(request.POST or None)
    context = {'form': form}
    if (form.is_valid() and request.method == 'POST'):
        form.save()
        return HttpResponseRedirect('/lab-4')
    return render(request, 'lab4_form.html', context)

def note_list(request):
    notes = Note.objects.all() 
    response = {'Notes': notes}
    return render(request, 'lab4_note_list.html', response)
