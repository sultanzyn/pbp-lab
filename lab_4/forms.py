from django.forms import ModelForm
from django import forms
from lab_2.models import Note
class NoteForm(ModelForm):
    class Meta:
        model = Note
        fields = '__all__'
        widget = {
            'to' : forms.TextInput(attrs={'class': 'form-control'}), 
            'From' : forms.TextInput(attrs={'class': 'form-control'}), 
            'title' : forms.TextInput(attrs={'class': 'form-control'}),
            'message' : forms.Textarea(attrs={'class': 'form-control', 'id':'message'}),
        }
